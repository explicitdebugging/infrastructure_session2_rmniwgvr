# [CrossWord]

## Statement
You are in the process of creating a crossword puzzle. You've already designed the board, but now you need to come up with words of various sizes to put in the puzzle. An empty crossword puzzle consists of filled squares ('X') and empty squares ('.').  Here is an example of a board with 5 rows and 6 columns:

X....X
X.XX.X
...X..
X.XX.X
..X...

A "slot" of length N is defined as exactly N empty squares in a row, surrounded on either side by either a filled square or the edge of the board.  N must be at least 2.  There are five horizontal slots in the example puzzle above:

First row, length = 4
Third row, length = 3
Third row, length = 2
Fifth row, length = 2
Fifth row, length = 3

Given a String[] *board*, representing an empty crossword puzzle, and an int *size*, your method should return the number of horizontal slots in the puzzle that are exactly *size* characters in length. Each element of *board* represents one row of the puzzle.

## Definitions
- *Class*: `CrossWord`
- *Method*: `countWords`
- *Parameters*: `String[], int`
- *Returns*: `int`
- *Method signature*: `int countWords(String[] board, int size)`

## Constraints
- *board* will contain between 3 and 50 elements, inclusive.
- Each element of *board* will contain between 3 and 50 characters, inclusive.
- Each element of *board* will be the same length.
- *board* will consist of only '.' and 'X' characters, and will contain at least two '.' characters.
- All '.' characters in *board* will be connected horizontally or vertically.
- *size* will be between 2 and 50, inclusive.

## Examples
### Example 1
#### Input
<c>["X....X",<br /> "X.XX.X",<br /> "...X..",<br /> "X.XX.X",<br /> "..X..."],<br />3</c>
#### Output
<c>2</c>
#### Reason
The example from above. Note that there are two horizontal slots with length = 3.

### Example 2
#### Input
<c>["...X...",<br /> ".X...X.",<br /> "..X.X..",<br /> "X..X..X",<br /> "..X.X..",<br /> ".X...X.",<br /> "...X..."],<br />3</c>
#### Output
<c>6</c>
#### Reason
There are two slots of length 3 on both the first and seventh rows, and one slot each on the second and sixth rows, for a total of 6.

### Example 3
#### Input
<c>[".....X....X....",<br /> ".....X....X....",<br /> "..........X....",<br /> "....X....X.....",<br /> "...X....X....XX",<br /> "XXX...X....X...",<br /> ".....X....X....",<br /> ".......X.......",<br /> "....X....X.....",<br /> "...X....X...XXX",<br /> "XX....X....X...",<br /> ".....X....X....",<br /> "....X..........",<br /> "....X....X.....",<br /> "....X....X....."],<br />5</c>
#### Output
<c>8</c>
### Example 4
#### Input
<c>["...",<br /> "...",<br /> "..."],<br />50</c>
#### Output
<c>0</c>
### Example 5
#### Input
<c>["....",<br /> "....",<br /> "...."],<br />3</c>
#### Output
<c>0</c>

