import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class DiskSpaceSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        List<Integer> usedBoxed = (List<Integer>) reader.next(new TypeRef<List<Integer>>(){}.getType());
        int[] used = new int[usedBoxed.size()];
        for (int _i = 0; _i < usedBoxed.size(); ++_i)
            used[_i] = usedBoxed.get(_i);
        reader.next();
        
        List<Integer> totalBoxed = (List<Integer>) reader.next(new TypeRef<List<Integer>>(){}.getType());
        int[] total = new int[totalBoxed.size()];
        for (int _i = 0; _i < totalBoxed.size(); ++_i)
            total[_i] = totalBoxed.get(_i);
        reader.close();

        DiskSpace solver = new DiskSpace();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        writer.write(solver.minDrives(used, total));
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
