import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class MarginCalculatorSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        List<String> itemsBoxed = (List<String>) reader.next(new TypeRef<List<String>>(){}.getType());
        String[] items = new String[itemsBoxed.size()];
        for (int _i = 0; _i < itemsBoxed.size(); ++_i)
            items[_i] = itemsBoxed.get(_i);
        reader.close();

        MarginCalculator solver = new MarginCalculator();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        writer.write(solver.percent(items));
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
