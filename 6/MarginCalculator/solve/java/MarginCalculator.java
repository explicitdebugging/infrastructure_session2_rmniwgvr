public class MarginCalculator {
	
	public int percent(String[] items){
		Double result;
		double pricePaid=0;
		double cost=0;
		for (int i=0;i < items.length; i++){
		
			pricePaid=Double.parseDouble(items[i].substring(0, 5))+pricePaid;
			cost=Double.parseDouble(items[i].substring(7, 13))+cost;
			

		}
		result= ((pricePaid-cost)/pricePaid)*100;
		return result.intValue();
	}
}