import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class KiloManSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        List<Integer> patternBoxed = (List<Integer>) reader.next(new TypeRef<List<Integer>>(){}.getType());
        int[] pattern = new int[patternBoxed.size()];
        for (int _i = 0; _i < patternBoxed.size(); ++_i)
            pattern[_i] = patternBoxed.get(_i);
        reader.next();
        
        String jumps = (String) reader.next(String.class);
        reader.close();

        KiloMan solver = new KiloMan();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        writer.write(solver.hitsTaken(pattern, jumps));
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
